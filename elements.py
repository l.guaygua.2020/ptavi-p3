import smil
import sys


def main(file):
    object = smil.SMIL(file)
    for partes in object.elements():
        print(partes.name())


if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 elements.py <file>")
    main(sys.argv[1]) # main("karaoke.smil") #comprobamos con karaoke.smil
