#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import xml.dom.minidom


class Humor:

    scores = ["buenisimo", "bueno", "regular", "malo", "malisimo"]

    def __init__(self, path):
        self.path = path

    def read_joke(self, node):
        """Given a chiste DOM node, get its score and contents

        Returns a dictionary with 'score', 'question' and 'answer' as keys.
        """
        score = node.getAttribute('calificacion')
        questions = node.getElementsByTagName('pregunta')
        question = questions[0].firstChild.nodeValue.strip()
        answers = node.getElementsByTagName('respuesta')
        answer = answers[0].firstChild.nodeValue.strip()
        contents = {'score': score, 'question': question, 'answer': answer}
        return contents

    def jokes(self):
        """Produce a list with a dictionary for each joke"""

        document = xml.dom.minidom.parse(self.path)
        root = document.childNodes[0]
        if root.tagName != 'humor':
            raise Exception("Root element is not humor")
        jokes = document.getElementsByTagName('chiste')
        lists = {};

        for joke in jokes:
            texts = self.read_joke(joke)
            score = texts['score']
            try:
                lists[score].append(texts)
            except KeyError:
                lists[score] = [texts]

        result = []
        for score in self.scores:
            if score in lists:
                for element in lists[score]:
                    result.append(element)
        return result

def main(path):
    """Programa principal"""
    humor = Humor(path)
    for joke in humor.jokes():
        print(f"Calificación: {joke['score']}.")
        print(f" Respuesta: {joke['answer']}")
        print(f" Pregunta: {joke['question']}")
        print()

if __name__ == "__main__":
    main("chistes.xml")
