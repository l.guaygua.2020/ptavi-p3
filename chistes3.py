#!/usr/bin/python3

import xml.dom.minidom
import sys
def main(path): #la funcion main admite un parametro path
    """Programa principal"""
    document = xml.dom.minidom.parse('chistes.xml')
    if document.getElementsByTagName('humor'): #si el elemento raiz no se llama humor...
        jokes = document.getElementsByTagName('chiste')
        chistes = []
        for joke in jokes:
            score = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            question = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            answer = answers[0].firstChild.nodeValue.strip()
            chistes.append ((score, question, answer))
        #ordenar chistes segun clasificacion
        ordencalif = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
        for orden in ordencalif:
            for chiste_orden in chistes:
                orden1 = chiste_orden[0]
                if orden == orden1:
                    print(f"Calificación: {orden}.")
                    print(f" Pregunta: {(chiste_orden[1])}")
                    print(f" Respuesta: {chiste_orden[2]}\n") #\n para escribir una linea en blanco


    else:
                raise "Root element is not humor" #ultimo punto del ejercicio 4

if __name__ == "__main__":
    main("chistes.xml")
