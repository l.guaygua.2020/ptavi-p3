#basado en xml
#mirar el ejemplo de karaoke que esta junto a el primer chistes

import xml.dom.minidom
import xml.etree.ElementTree
import sys


class SMIL():

    def __init__(self, path):
        self.path = path

    def elements(self):
        ficha = xml.etree.ElementTree.parse(self.path)
        elementos = []
        tipo= []

        for elemento in ficha.iter():
            elementos.append(elemento)

        for tipo_elemento in elementos:
            tipo.append(Element(tipo_elemento.tag, tipo_elemento.attrib, elementos))

        return tipo
class Element():
    def __init__(self, elemento, diccionario, cadena):
        self.elemento = elemento
        self.diccionario = diccionario
        self.cadena = cadena


    def name(self):
        return self.elemento

    def attrs(self):
        return self.diccionario
